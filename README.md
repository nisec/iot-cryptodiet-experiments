# IoT-CryptoDiet Experiments

A lightweight cryptographic library for securing communication protocols between multiple communicating nodes without the need for an external trusted entity. 

# Experiments Overview

The experiments implemented in this project focus on evaluating the performance of the core cryptographic schemes of our work on the constrained Zolertia Re-mote revb boards. The evaluation focuses on the code size, data size, memory usage, and energy consumption.

## dependencies

The functionalities of the scripts in this work are built on top of the Contiki-NG IoT operating system. To this end, to successfully run the experiments on the sensor device, Contiki-NG has to be downloaded from https://github.com/contiki-ng/contiki-ng. This repository should be copied into the "contiki-ng/examples/" directory for use. Code in this section is written in the C programming language. All the encryption schemes utilized are based on the Tinycrypt cryptograhic library.

* `ecc.c`: implementation of the Elliptic Curve Cryptography (ECC) encryption scheme 
* `ecc_dh.c` : implementation of the ECC - Diffie Hellman (DH) scheme
* `ecc_dsa.c` : implementation of the ECC - Digital Signing Algorithm (DSA) scheme
* `myencrypt.c` : implementation of the AES 128 encryption scheme
* `sha256.c` : implementation of the SHA 256 hashing scheme

Tinycrypt library: https://github.com/intel/tinycrypt

# Run sensor experiments

* Specifiy the IoT sensor board and target platform being used for the experiments. This work uses the zolertia re-mote revb board which is based on the "zoul" target platform"\
    `make TARGET=zoul BOARD=remote-revb savetarget`
    
* Run the the script to run through a number of iterations of the core cryptographic components. This includes ECC keypair generation, generating a shared key, AES encryption an decryption, and ECDSA signing and verification. Take note of the usb port that the IoT device is connected to (e.g. USB1):\
    `make core_function_test.upload PORT="/dev/ttyUSB1"`

* Log into the sensor to view results of the tests:\
    `make login`

## protocol based approach between two sensors
* Take note of the usb ports for each device. For example, USB1 and USB2.
* On the first device, run: \
    `make secure_client_one.upload PORT="/dev/ttyUSB1"`

* On the second device, run: \
    `make secure_client_two.upload PORT="/dev/ttyUSB2"`

* Log in to each sensor to view the interactions: \
    `make login PORT="respective USB port"`

# Credits

## Authors

* Eugene Frimpong (Tampere University, Tampere, Finland)
* Antonios Michalas (Tampere University, Tampere, Finland)

## Funding

This research has received funding from the European Union’s Horizon 2020 research and innovation Programme under grant agreement No 825355 (CYBELE).