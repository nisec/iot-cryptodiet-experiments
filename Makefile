CONTIKI_PROJECT = experiments.c

all: $(CONTIKI_PROJECT)

PROJECT_SOURCEFILES += ecc.c sha256.c ecc_dsa.c ecc_dh.c test_ecc_utils.c myencrypt.c

CONTIKI = ../..
include $(CONTIKI)/Makefile.include