#ifndef __TC_ECC_DSA_H__
#define __TC_ECC_DSA_H__

#include <ecc.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Generate an ECDSA signature for a given hash value.
 * @return returns TC_CRYPTO_SUCCESS (1) if the signature generated successfully
 *         returns TC_CRYPTO_FAIL (0) if an error occurred.
 *
 * @param p_private_key IN -- Your private key.
 * @param p_message_hash IN -- The hash of the message to sign.
 * @param p_hash_size IN -- The size of p_message_hash in bytes.
 * @param p_signature OUT -- Will be filled in with the signature value. Must be
 * at least 2 * curve size long (for secp256r1, signature must be 64 bytes long).
 *
 * @warning A cryptographically-secure PRNG function must be set (using
 * uECC_set_rng()) before calling uECC_sign().
 * @note Usage: Compute a hash of the data you wish to sign (SHA-2 is
 * recommended) and pass it in to this function along with your private key.
 * @note side-channel countermeasure: algorithm strengthened against timing
 * attack.
 */
int uECC_sign(const uint8_t *p_private_key, const uint8_t *p_message_hash,
	      unsigned p_hash_size, uint8_t *p_signature, uECC_Curve curve);

#ifdef ENABLE_TESTS
/*
 * THIS FUNCTION SHOULD BE CALLED FOR TEST PURPOSES ONLY.
 * Refer to uECC_sign() function for real applications.
 */
int uECC_sign_with_k(const uint8_t *private_key, const uint8_t *message_hash,
		     unsigned int hash_size, uECC_word_t *k, uint8_t *signature,
		     uECC_Curve curve);
#endif

/**
 * @brief Verify an ECDSA signature.
 * @return returns TC_SUCCESS (1) if the signature is valid
 * 	   returns TC_FAIL (0) if the signature is invalid.
 *
 * @param p_public_key IN -- The signer's public key.
 * @param p_message_hash IN -- The hash of the signed data.
 * @param p_hash_size IN -- The size of p_message_hash in bytes.
 * @param p_signature IN -- The signature values.
 *
 * @note Usage: Compute the hash of the signed data using the same hash as the
 * signer and pass it to this function along with the signer's public key and
 * the signature values (hash_size and signature).
 */
int uECC_verify(const uint8_t *p_public_key, const uint8_t *p_message_hash,
		unsigned int p_hash_size, const uint8_t *p_signature, uECC_Curve curve);

#ifdef __cplusplus
}
#endif

#endif /* __TC_ECC_DSA_H__ */
